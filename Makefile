test:
	@go vet ./...
	@go test \
	  -v \
	  -failfast \
	  -count=1 \
	  -race \
	  -coverpkg=./... \
	  -coverprofile coverage.out \
	  ./...
	@go tool cover -html=coverage.out -o coverage.html

build:
	go build -o eventlooper .
