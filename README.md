# eventlooper

[![Build Status](https://travis-ci.com/siadat/eventlooper.svg?branch=master)](https://travis-ci.com/siadat/eventlooper)
[![codecov](https://codecov.io/gl/siadat/eventlooper/branch/master/graph/badge.svg)](https://codecov.io/gl/siadat/eventlooper)

**eventlooper** is both a language and an interpreter for that language.

## Usage

```shell
make build
./eventlooper FILENAME
```

## Examples

### Hello World
Source:
```
# Hello world example:
main() {
  PRINT("Hello, world!")
}
```

Output:
```
[main:1] Hello, world!
```

### Sequential functions
Source:
```
main() {
  f()
  g()
}

f() {
  PRINT("f")
}

g() {
  PRINT("g")
}

```
Output:
```
[main:1] [f:1] f
[main:2] [g:1] g
```

### Nested functions (CallStack)

Source:
```
main() {
    PRINT("m1")
    f()
    PRINT("m2")
}

f() {
    PRINT("f1")
    g()
    PRINT("f2")
}

g() {
    PRINT("g1")
}
```

Output:
```
[main:1] m1
[main:2] [f:1] f1
[main:2] [f:2] [g:1] g1
[main:2] [f:3] f2
[main:3] m2
```

### Concurrency
Source:
```
main() {
  GO f()
  GO g()
}

f() {
  PRINT("start")
  SLEEP(1)
  PRINT("end")
}

g() {
  PRINT("start")
  SLEEP(1)
  PRINT("end")
}
```

Output:
```
[f:1] start
[f:2] SLEEP 1
[g:1] start
[g:2] SLEEP 1
[f:3] end
[g:3] end
```

### Error messages

Scan errors are human readable:
```
error: scan failed
  PR!NT()
    ^
    |
    In line 2, bad token '!'
```

Parse errors are human readable:

```
error: panic: parse failed
  PRINT())
         ^
         |
         In line 2, expected Ident; got CloseParen:")"
```

### Testfiles

The [testfiles/](testfiles/) directory contains test scripts that are part of
the test suite, they are run by the CI and their expected output is included as
comments using exact matching and regular expressions.

```
main() {
    PRINT("Hello, world!")
    SHELL("echo Today is $(date +%F)")
}

# Output: [main:1] Hello, world!
# Regexp: \[main:2\] Today is 20\d{2}-\d{2}-\d{2}
```

This file is run and its output is asserted to match the `# Output: ...` and `# Regexp: ...` comments.

You can run them manually:
```
./eventlooper testfiles/test1.el
```

## Internals

### Spec

Grammar:

```
File        = { FuncDecl } .
FuncDecl    = Identifier "(" ")" "{" Statements "}" .
Statements  = { Statement ";" } .
Statement   = Identifier "(" [ Literal ] ")" | GoStatement .
GoStatement = "GO" Identifier "(" ")" .
Literal     = { String | Int } .
Identifier  = Letter { Letter | Int } .
Letter      = ("a"…"z") | ("A"…"Z") .
Int         = "0" | ( "1"…"9" ) [ Int ] .
String      = '"' { Char } '"' .
```

Keywords:

```
GO
```

Builtin functions:

```
SLEEP SHELL GET
```

### Scanning and Parsing

- The Scanner reads the source files and returns a list of Tokens.
  Each Token records information about the type of the token, its literal value, and its position in the source file.
  The position is used for producing human readable errors messages in the scanner and the parser.
- The Parser reads the tokens returned by the scanner and builds a list of FuncDecls and Comments.
- A FuncDecl is a definition of a function defined by the user in the script.
- Each script must contain an entry-point function named `main`.

### Interpreting

- The Scheduler decides what function can continue its execution.
- `SLEEP(2)` stops execution, allowing the Scheduler to pass execution to another concurrent function.
- `PRINT("...")` function prints to an io.Writer
- A Call is a function invokation, eg f().
- A CallStack is a call and all its parent calls leading to it.
  For example, if f calls g, and g calls h, the CallStack when we are in h is {f, g, h}.
  We need to keep track of the parent Calls because we should be able to continue the execution
  of each Call (and its parents) when the scheduler allows us to continue after a SLEEP.
- `GO f()` starts f() in a new CallStack.
