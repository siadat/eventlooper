package main

import (
	"fmt"
	"path/filepath"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/siadat/eventlooper/parser"
)

func TestScheduler(t *testing.T) {
	for _, tt := range []struct {
		src  string
		want []string
		err  error
	}{
		{
			src: `
	f() {
		PRINT("f: 1")
		PRINT("f: 2")
	}

	main() {
		PRINT("main: 1")
		f()
		PRINT("main: 2")
	}
		
	`,
			want: []string{
				"[main:1] main: 1",
				"[main:2] [f:1] f: 1",
				"[main:2] [f:2] f: 2",
				"[main:3] main: 2",
			},
		},
		{
			src: `
	f() {
		PRINT("f: 1")
		SLEEP(3)
		PRINT("f: 2")
	}

	main() {
		PRINT("main: 1")
		f()
		PRINT("main: 2")
	}
		
	`,
			want: []string{
				"[main:1] main: 1",
				"[main:2] [f:1] f: 1",
				"[main:2] [f:2] SLEEP 3",
				"[main:2] [f:2] SLEEP 2",
				"[main:2] [f:2] SLEEP 1",
				"[main:2] [f:3] f: 2",
				"[main:3] main: 2",
			},
		},
		{
			src: `f() { }`,
			err: fmt.Errorf(`undefined function "main"`),
		},
		{
			src: `
	f() {
		PRINT("f: 1")
		SLEEP(0)
		PRINT("f: 2")
	}

	main() {
		PRINT("main: 1")
		f()
		PRINT("main: 2")
	}
		
	`,
			want: []string{
				"[main:1] main: 1",
				"[main:2] [f:1] f: 1",
				"[main:2] [f:3] f: 2",
				"[main:3] main: 2",
			},
		},
		{
			src: `f() { }`,
			err: fmt.Errorf(`undefined function "main"`),
		},
	} {
		var lw LineWriter
		program, err := parser.ParseSrc(tt.src)
		require.NoError(t, err)

		scheduler := &Scheduler{}
		err = scheduler.Start(&program, &lw)
		require.Equal(t, tt.err, err)

		require.Equal(t, tt.want, lw.Lines, tt.src)
	}
}

func TestEventlooper(t *testing.T) {
	funcDeclMain := parser.FuncDecl{
		Name: "main",
		Statements: []parser.Statement{
			parser.GoStatement{Name: "func0", Arg: nil},
			parser.GoStatement{Name: "func1", Arg: nil},
			parser.GoStatement{Name: "func2", Arg: nil},
		},
	}
	funcDecl0 := parser.FuncDecl{
		Name: "func0",
	}
	funcDecl1 := parser.FuncDecl{
		Name: "func1",
		Statements: []parser.Statement{
			parser.CallStatement{Name: "PRINT", Arg: parser.TypeString("print1")},
			parser.CallStatement{Name: "PRINT", Arg: parser.TypeString("print2")},
			parser.CallStatement{Name: "SLEEP", Arg: parser.TypeInt(2)},
			parser.CallStatement{Name: "PRINT", Arg: parser.TypeString("print3")},
		},
	}
	funcDecl2 := parser.FuncDecl{
		Name: "func2",
		Statements: []parser.Statement{
			parser.CallStatement{Name: "PRINT", Arg: parser.TypeString("print1")},
			parser.CallStatement{Name: "PRINT", Arg: parser.TypeString("print2")},
			parser.CallStatement{Name: "SLEEP", Arg: parser.TypeInt(2)},
			parser.CallStatement{Name: "PRINT", Arg: parser.TypeString("print3")},
			parser.CallStatement{Name: "PRINT", Arg: parser.TypeString("print4")},
		},
	}

	funcDecls := []parser.FuncDecl{
		funcDeclMain,
		funcDecl0,
		funcDecl1,
		funcDecl2,
	}

	want := []string{
		"[func1:1] print1",
		"[func1:2] print2",
		"[func1:3] SLEEP 2",

		"[func2:1] print1",
		"[func2:2] print2",
		"[func2:3] SLEEP 2",

		"[func1:3] SLEEP 1",
		"[func2:3] SLEEP 1",

		"[func1:4] print3",

		"[func2:4] print3",
		"[func2:5] print4",
	}

	var lw LineWriter

	program := parser.Program{
		FuncDecls: funcDecls,
	}
	scheduler := &Scheduler{}
	err := scheduler.Start(&program, &lw)
	require.NoError(t, err)
	require.Equal(t, want, lw.Lines)
}

type LineWriter struct {
	Lines []string
}

func (lw *LineWriter) Write(p []byte) (n int, err error) {
	s := strings.TrimRight(string(p), "\n")
	lines := strings.Split(s, "\n")
	lw.Lines = append(lw.Lines, lines...)
	return len(p), nil
}

func TestTestfiles(t *testing.T) {
	filenames, err := filepath.Glob("./testfiles/*.el")
	require.NoError(t, err)
	for _, filename := range filenames {
		if strings.HasPrefix(filepath.Base(filename), "_") {
			continue
		}
		program, err := parser.ParseFile(filename)
		require.NoError(t, err)

		var lw LineWriter
		scheduler := &Scheduler{}

		_ = scheduler.Start(&program, &lw)

		var i int = 0
		var checks []string
		for _, cm := range program.Comments {
			if strings.HasPrefix(cm, "# Output: ") {
				checks = append(checks, cm)
			} else if strings.HasPrefix(cm, "# Regexp: ") {
				checks = append(checks, cm)
			}
		}

		require.Equal(t, len(checks), len(lw.Lines), strings.Join(lw.Lines, "\n"))

		for _, ch := range checks {
			if strings.HasPrefix(ch, "# Output: ") {
				want := strings.TrimSuffix(strings.TrimPrefix(ch, "# Output: "), "\n")
				require.Equal(t, want, lw.Lines[i])
				i++
			} else if strings.HasPrefix(ch, "# Regexp: ") {
				want := strings.TrimSuffix(strings.TrimPrefix(ch, "# Regexp: "), "\n")
				require.Regexp(t, want, lw.Lines[i])
				i++
			}
		}

	}

}
