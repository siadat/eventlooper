package scanner

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/siadat/eventlooper/tokens"
)

const IgnorePos = -1

func TestScanner(t *testing.T) {
	for _, tt := range []struct {
		src  string
		err  error
		want []tokens.Token
	}{
		{
			src:  ``,
			err:  nil,
			want: []tokens.Token{},
		},
		{
			src: `func0() {}`,
			err: nil,
			want: []tokens.Token{
				tokens.NewToken(tokens.TypeIdent, "func0", 0),
				tokens.NewToken(tokens.TypeOpenParen, "(", 5),
				tokens.NewToken(tokens.TypeCloseParen, ")", 6),
				tokens.NewToken(tokens.TypeOpenBrace, "{", 8),
				tokens.NewToken(tokens.TypeNewline, "\n", 9),
				tokens.NewToken(tokens.TypeCloseBrace, "}", 9),
			},
		},
		{
			src: `main() { PRINT("123\""); SLEEP(456); func0() }`,
			err: nil,
			want: []tokens.Token{
				// tokens.NewToken(tokens.TypeNewline, "\n", IgnorePos),
				tokens.NewToken(tokens.TypeIdent, "main", IgnorePos),
				tokens.NewToken(tokens.TypeOpenParen, "(", IgnorePos),
				tokens.NewToken(tokens.TypeCloseParen, ")", IgnorePos),
				tokens.NewToken(tokens.TypeOpenBrace, "{", IgnorePos),
				// tokens.NewToken(tokens.TypeNewline, "\n", IgnorePos),
				tokens.NewToken(tokens.TypeIdent, "PRINT", IgnorePos),
				tokens.NewToken(tokens.TypeOpenParen, "(", IgnorePos),
				tokens.NewToken(tokens.TypeString, `"123\""`, IgnorePos),
				tokens.NewToken(tokens.TypeCloseParen, ")", IgnorePos),
				tokens.NewToken(tokens.TypeNewline, "\n", IgnorePos),
				tokens.NewToken(tokens.TypeIdent, "SLEEP", IgnorePos),
				tokens.NewToken(tokens.TypeOpenParen, "(", IgnorePos),
				tokens.NewToken(tokens.TypeNumber, `456`, IgnorePos),
				tokens.NewToken(tokens.TypeCloseParen, ")", IgnorePos),
				tokens.NewToken(tokens.TypeNewline, "\n", IgnorePos),
				tokens.NewToken(tokens.TypeIdent, "func0", IgnorePos),
				tokens.NewToken(tokens.TypeOpenParen, "(", IgnorePos),
				tokens.NewToken(tokens.TypeCloseParen, ")", IgnorePos),
				tokens.NewToken(tokens.TypeNewline, "\n", IgnorePos),
				tokens.NewToken(tokens.TypeCloseBrace, "}", IgnorePos),
			},
		},
		{
			src: `
			main() {
				PRINT("123\"")
				SLEEP(456)
				func0()
			}`,
			err: nil,
			want: []tokens.Token{
				tokens.NewToken(tokens.TypeNewline, "\n", IgnorePos),
				tokens.NewToken(tokens.TypeIdent, "main", IgnorePos),
				tokens.NewToken(tokens.TypeOpenParen, "(", IgnorePos),
				tokens.NewToken(tokens.TypeCloseParen, ")", IgnorePos),
				tokens.NewToken(tokens.TypeOpenBrace, "{", IgnorePos),
				// tokens.NewToken(tokens.TypeNewline, "\n", IgnorePos),
				tokens.NewToken(tokens.TypeNewline, "\n", IgnorePos),
				tokens.NewToken(tokens.TypeIdent, "PRINT", IgnorePos),
				tokens.NewToken(tokens.TypeOpenParen, "(", IgnorePos),
				tokens.NewToken(tokens.TypeString, `"123\""`, IgnorePos),
				tokens.NewToken(tokens.TypeCloseParen, ")", IgnorePos),
				tokens.NewToken(tokens.TypeNewline, "\n", IgnorePos),
				tokens.NewToken(tokens.TypeIdent, "SLEEP", IgnorePos),
				tokens.NewToken(tokens.TypeOpenParen, "(", IgnorePos),
				tokens.NewToken(tokens.TypeNumber, `456`, IgnorePos),
				tokens.NewToken(tokens.TypeCloseParen, ")", IgnorePos),
				tokens.NewToken(tokens.TypeNewline, "\n", IgnorePos),
				tokens.NewToken(tokens.TypeIdent, "func0", IgnorePos),
				tokens.NewToken(tokens.TypeOpenParen, "(", IgnorePos),
				tokens.NewToken(tokens.TypeCloseParen, ")", IgnorePos),
				tokens.NewToken(tokens.TypeNewline, "\n", IgnorePos),
				tokens.NewToken(tokens.TypeNewline, "\n", IgnorePos),
				tokens.NewToken(tokens.TypeCloseBrace, "}", IgnorePos),
			},
		},
		{
			src: `func0() {
			  # comment... () {} , 123
			}`,
			err: nil,
			want: []tokens.Token{
				tokens.NewToken(tokens.TypeIdent, "func0", IgnorePos),
				tokens.NewToken(tokens.TypeOpenParen, "(", IgnorePos),
				tokens.NewToken(tokens.TypeCloseParen, ")", IgnorePos),
				tokens.NewToken(tokens.TypeOpenBrace, "{", IgnorePos),
				// tokens.NewToken(tokens.TypeNewline, "\n", IgnorePos),
				tokens.NewToken(tokens.TypeNewline, "\n", IgnorePos),
				tokens.NewToken(tokens.TypeComment, "# comment... () {} , 123\n", IgnorePos),
				tokens.NewToken(tokens.TypeNewline, "\n", IgnorePos),
				tokens.NewToken(tokens.TypeCloseBrace, "}", IgnorePos),
			},
		},
		{
			src: `
# comment
!
			`,
			err:  fmt.Errorf("scan failed\n!\n^\n|\nIn line 3, bad token '!'\n"),
			want: []tokens.Token{},
		},
	} {
		got, err := ScanSrc(tt.src)
		require.Equal(t, tt.err, err, tt.src)

		require.Equal(t, len(tt.want), len(got), fmt.Sprintf("Src: %s\nWant: %v\nGot: %v", tt.src, tt.want, got))
		// require.Equal(t, tt.want, got, tt.src)
		for i := range got {
			if tt.want[i].Pos != IgnorePos {
				require.Equal(t, tt.want[i], got[i], tt.src)
			} else {
				require.Equal(t, tt.want[i].Typ, got[i].Typ, tt.src)
				require.Equal(t, tt.want[i].Lit, got[i].Lit, tt.src)
			}
		}
	}
}
