package scanner

import (
	"fmt"
	"regexp"
	"strings"

	"gitlab.com/siadat/eventlooper/tokens"
)

func AnnotateSrc(src string, pos int, msg, annotation string) string {
	line, row, col := getLine(src, pos)
	leftPadding := strings.Repeat(" ", col-1)
	var b strings.Builder
	b.WriteString(msg + "\n")
	b.WriteString(fmt.Sprintf("%s\n", line))
	b.WriteString(fmt.Sprintf("%s^\n", leftPadding))
	b.WriteString(fmt.Sprintf("%s|\n", leftPadding))
	b.WriteString(fmt.Sprintf("%sIn line %d, %s\n", leftPadding, row, annotation))
	return b.String()
}

func getLine(src string, pos int) (line string, row, col int) {
	lines := strings.Split(src, "\n")

	cursor := 0
	for i := 0; i < len(lines); i++ {
		line := lines[i]

		if cursor+len(line) >= pos {
			col = pos - cursor + 1
			row = i + 1
			return line, row, col
		}

		cursor += len(line) + 1 // 1 == '\n"
	}

	return "", 0, 0
}

func ScanSrc(src string) ([]tokens.Token, error) {
	pos := 0
	ttokens := []tokens.Token{}

	for {
		if pos >= len(src) {
			break
		}

		if m := regexp.MustCompile(`^#[^\n\r]*[\n\r]?`).FindStringSubmatch(src[pos:]); len(m) > 0 {
			ttokens = append(ttokens, tokens.NewToken(tokens.TypeComment, m[0], pos))
			pos += len(m[0])
		} else if m := regexp.MustCompile(`^"(\\.|[^"])*"`).FindStringSubmatch(src[pos:]); len(m) > 0 {
			ttokens = append(ttokens, tokens.NewToken(tokens.TypeString, m[0], pos))
			pos += len(m[0])
		} else if m := regexp.MustCompile(`^(0|[1-9]([0-9]*(\.[0-9]+)?)?)\b`).FindStringSubmatch(src[pos:]); len(m) > 0 {
			ttokens = append(ttokens, tokens.NewToken(tokens.TypeNumber, m[0], pos))
			pos += len(m[0])
		} else if src[pos] == '\n' {
			ttokens = append(ttokens, tokens.NewToken(tokens.TypeNewline, "\n", pos))
			pos++
		} else if src[pos] == '(' {
			ttokens = append(ttokens, tokens.NewToken(tokens.TypeOpenParen, "(", pos))
			pos++
		} else if src[pos] == ')' {
			ttokens = append(ttokens, tokens.NewToken(tokens.TypeCloseParen, ")", pos))
			pos++
		} else if src[pos] == '{' {
			ttokens = append(ttokens, tokens.NewToken(tokens.TypeOpenBrace, "{", pos))
			pos++
		} else if src[pos] == '}' {
			ttokens = append(ttokens, tokens.NewToken(tokens.TypeNewline, "\n", pos))
			ttokens = append(ttokens, tokens.NewToken(tokens.TypeCloseBrace, "}", pos))
			pos++
		} else if src[pos] == ';' {
			ttokens = append(ttokens, tokens.NewToken(tokens.TypeNewline, "\n", pos))
			pos++
		} else if src[pos] == '*' {
			ttokens = append(ttokens, tokens.NewToken(tokens.TypeMult, "*", pos))
			pos++
		} else if src[pos] == '-' {
			ttokens = append(ttokens, tokens.NewToken(tokens.TypeMult, "-", pos))
			pos++
		} else if m := regexp.MustCompile(`^[ \t]+`).FindStringSubmatch(src[pos:]); len(m) > 0 {
			pos += len(m[0])
		} else if m := regexp.MustCompile(`^(==)`).FindStringSubmatch(src[pos:]); len(m) > 0 {
			ttokens = append(ttokens, tokens.NewToken(tokens.TypeEqual, m[0], pos))
			pos += len(m[0])
		} else if m := regexp.MustCompile(`^(!=)`).FindStringSubmatch(src[pos:]); len(m) > 0 {
			ttokens = append(ttokens, tokens.NewToken(tokens.TypeEqual, m[0], pos))
			pos += len(m[0])
		} else if m := regexp.MustCompile(`^(GO)\s`).FindStringSubmatch(src[pos:]); len(m) > 0 {
			ttokens = append(ttokens, tokens.NewToken(tokens.TypeIdent, m[1], pos))
			pos += len(m[1])
		} else if m := regexp.MustCompile(`^[a-zA-Z][a-zA-Z0-9]*`).FindStringSubmatch(src[pos:]); len(m) > 0 {
			ttokens = append(ttokens, tokens.NewToken(tokens.TypeIdent, m[0], pos))
			pos += len(m[0])
		} else {
			return nil, fmt.Errorf(AnnotateSrc(src, pos, "scan failed", fmt.Sprintf("bad token '%c'", src[pos])))
		}
	}

	return ttokens, nil
}
