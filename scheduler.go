package main

import (
	"fmt"
	"io"

	"gitlab.com/siadat/eventlooper/parser"
)

type Scheduler struct {
	concurrencyq []*CallStack // FIFO
}

func (sch *Scheduler) Start(program *parser.Program, output io.Writer) error {
	mainFuncDecl, err := program.GetFunc("main")
	if err != nil {
		return err
	}
	sch.push(&CallStack{
		Calls: []*Call{
			{FuncDecl: mainFuncDecl},
		},
	})

	for len(sch.concurrencyq) > 0 {
		cs := sch.concurrencyq[0]
		sch.pop()

		if cs.isDone() {
			continue
		}

		newCallStacks, err := cs.proceed(program, output)
		if err != nil {
			return fmt.Errorf("runtime error: %s\n", err)
		}
		for _, ncs := range newCallStacks {
			sch.push(ncs)
		}

		if !cs.isDone() {
			sch.push(cs)
		}
	}
	return nil
}

func (sch *Scheduler) push(cs *CallStack) {
	sch.concurrencyq = append(sch.concurrencyq, cs)
}

func (sch *Scheduler) pop() {
	sch.concurrencyq = sch.concurrencyq[1:]
}
