module gitlab.com/siadat/eventlooper

go 1.15

require github.com/stretchr/testify v1.6.1

replace (
	gitlab.com/siadat/eventlooper/parser => ./parser
	gitlab.com/siadat/eventlooper/scanner => ./scanner
	gitlab.com/siadat/eventlooper/tokens => ./tokens
)
