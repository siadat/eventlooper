main() {
    PRINT("m1")
    f()
    PRINT("m2")
}

f() {
    PRINT("f1")
    g()
    PRINT("f2")
}

g() {
    PRINT("g1")
}

# Output: [main:1] m1
# Output: [main:2] [f:1] f1
# Output: [main:2] [f:2] [g:1] g1
# Output: [main:2] [f:3] f2
# Output: [main:3] m2
