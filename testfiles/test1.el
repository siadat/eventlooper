main() {
    f()
    g()
}

f() {
    PRINT("1")
    PRINT("2")
}

g() {
    PRINT("1")
    PRINT("2")
}

# Output: [main:1] [f:1] 1
# Output: [main:1] [f:2] 2
# Output: [main:2] [g:1] 1
# Output: [main:2] [g:2] 2
