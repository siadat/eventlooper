main() {
    GO f()
    GO g()
}

f() {
    PRINT("f")
    g()
}

g() {
    PRINT("g")
    f()
}

# Output: [f:1] f
# Output: [f:2] [g:1] g
# Output: [f:2] [g:2] [f:1] f
# Output: [f:2] [g:2] [f:2] [g:1] g
# Output: [f:2] [g:2] [f:2] [g:2] [f:1] f
# Output: [f:2] [g:2] [f:2] [g:2] [f:2] [g:1] g
# Output: [f:2] [g:2] [f:2] [g:2] [f:2] [g:2] [f:1] f
# Output: [f:2] [g:2] [f:2] [g:2] [f:2] [g:2] [f:2] [g:1] g
# Output: [f:2] [g:2] [f:2] [g:2] [f:2] [g:2] [f:2] [g:2] [f:1] f
# Output: [f:2] [g:2] [f:2] [g:2] [f:2] [g:2] [f:2] [g:2] [f:2] [g:1] g
# Output: [f:2] [g:2] [f:2] [g:2] [f:2] [g:2] [f:2] [g:2] [f:2] [g:2] [f:1] f
