main() {
    PRINT("Hello, world!")
    SHELL("echo Today is $(date +%F)")
}

# Output: [main:1] Hello, world!
# Regexp: \[main:2\] Today is 20\d{2}-\d{2}-\d{2}
