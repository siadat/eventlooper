main() {
    PRINT("main start")
    GO f()
    GO g()
    GO h()
    PRINT("main end")
}

f() {}

g() {
    PRINT("start")
    PRINT("work...")
    SLEEP(2)
    PRINT("end")
}

h() {
    PRINT("start")
    PRINT("work...")
    SLEEP(2)
    PRINT("work...")
    PRINT("end")
}


# Output: [main:1] main start
# Output: [main:5] main end
# Output: [g:1] start
# Output: [g:2] work...
# Output: [g:3] SLEEP 2
# Output: [h:1] start
# Output: [h:2] work...
# Output: [h:3] SLEEP 2
# Output: [g:3] SLEEP 1
# Output: [h:3] SLEEP 1
# Output: [g:4] end
# Output: [h:4] work...
# Output: [h:5] end
