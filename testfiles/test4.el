main() {
    # The SHELL command:
    SHELL("echo Hello, $USER!")
    # PRINT("Here is an http request:")
    # GET("http://ip.jsontest.com/")
}

# Regexp: \[main:1\] Hello, \S*!
# # Output: [main:2] Here is an http request:
# # Regexp: \[main:3\] {"ip": "\d+\.\d+\.\d+\.\d+"}
