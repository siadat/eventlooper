package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os/exec"
	"strings"

	"gitlab.com/siadat/eventlooper/parser"
)

type Call struct {
	FuncDecl parser.FuncDecl

	cursor          int
	remainingSleeps int
}

type CallStack struct {
	Calls []*Call // FILO
}

func (cs *CallStack) push(call *Call) error {
	if len(cs.Calls) > 10 {
		return fmt.Errorf("call stack overflow")
	}
	cs.Calls = append(cs.Calls, call)
	return nil
}

func (cs *CallStack) pop() {
	cs.Calls = cs.Calls[:len(cs.Calls)-1]
}

func (call *Call) isDone() bool {
	return call.cursor == len(call.FuncDecl.Statements)
}

func (cs *CallStack) isDone() bool {
	return len(cs.Calls) == 0
}

func (call *Call) String() string {
	if call.remainingSleeps > 0 {
		// add +1 to cursor, because cursor is 1 behind when sleeping
		// (the reason cursor is 1 behind when sleeping is because we
		// evaluate the sleep command until remainingSleeps==0)
		return fmt.Sprintf("[%s:%d]", call.FuncDecl.Name, call.cursor+1)
	} else {
		return fmt.Sprintf("[%s:%d]", call.FuncDecl.Name, call.cursor)
	}

}

func (cs *CallStack) String() string {
	names := make([]string, 0, len(cs.Calls))
	for _, call := range cs.Calls {
		names = append(names, fmt.Sprintf("%s", call.String()))
	}
	return strings.Join(names, " ")
}

func (call *Call) sleepOnce() string {
	line := fmt.Sprintf("SLEEP %d", call.remainingSleeps)
	call.remainingSleeps--
	if call.remainingSleeps == 0 {
		call.cursor++
	}
	return line
}

func (cs *CallStack) proceed(program *parser.Program, output io.Writer) (dispatchedCallStacks []*CallStack, err error) {
	for {
		statement, call, ok := cs.nextStatement(program)
		if !ok {
			return
		}

		if call.remainingSleeps > 0 {
			fmt.Fprintf(output, "%s %s\n", cs, call.sleepOnce())
			return
		}

		switch stmt := statement.(type) {
		case parser.GoStatement:
			{
				call.cursor++

				switch stmt.Name {
				case "SLEEP", "PRINT":
					return nil, fmt.Errorf("GO is not supported with builtin functions (ie PRINT, SLEEP, GO, SHELL, GET)")
				}

				f, err := program.GetFunc(stmt.Name)
				if err != nil {
					return nil, err
				}

				dispatchedCallStacks = append(dispatchedCallStacks, &CallStack{
					Calls: []*Call{
						{FuncDecl: f},
					},
				})
			}
		case parser.CallStatement:
			switch stmt.Name {
			case "SLEEP":
				{
					sleepValue, ok := stmt.Arg.(parser.TypeInt)
					if !ok {
						return nil, fmt.Errorf("expected int; got %q", stmt.Arg)
					}
					call.remainingSleeps = int(sleepValue)

					if call.remainingSleeps > 0 {
						fmt.Fprintf(output, "%s %s\n", cs, call.sleepOnce())
						return
					}

					call.cursor++
				}
			case "PRINT":
				{
					call.cursor++
					fmt.Fprintf(output, "%s %s\n", cs, stmt.Arg.(parser.TypeString))
				}
			case "SHELL":
				{
					cmd := exec.Command("sh", "-c", string(stmt.Arg.(parser.TypeString)))
					stdoutStderr, err := cmd.CombinedOutput()
					if err != nil {
						return nil, err
					}
					line := strings.TrimSpace(string(stdoutStderr))

					call.cursor++
					fmt.Fprintf(output, "%s %s\n", cs, line)
				}
			case "GET":
				{
					resp, err := http.Get(string(stmt.Arg.(parser.TypeString)))
					if err != nil {
						return nil, fmt.Errorf("get failed: %s", err)
					}

					defer resp.Body.Close()
					body, err := ioutil.ReadAll(resp.Body)
					if err != nil {
						return nil, fmt.Errorf("get read failed: %s", err)
					}

					call.cursor++
					fmt.Fprintf(output, "%s %s\n", cs, string(body))
				}
			default:
				{
					call.cursor++
					f, err := program.GetFunc(stmt.Name)
					if err != nil {
						return nil, err
					}

					call := &Call{FuncDecl: f}
					if err := cs.push(call); err != nil {
						return nil, err
					}
				}
			}
		}
	}
}

func (cs *CallStack) nextStatement(program *parser.Program) (parser.Statement, *Call, bool) {
	for {
		if len(cs.Calls) == 0 {
			// nothing left in the call stack
			return nil, nil, false
		}

		lastCall := cs.Calls[len(cs.Calls)-1]
		if lastCall.isDone() {
			cs.pop()
			continue
		}

		stmt := lastCall.FuncDecl.Statements[lastCall.cursor]
		return stmt, lastCall, true
	}
}
