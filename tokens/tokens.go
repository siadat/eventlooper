package tokens

import "fmt"

type TokenType int

const (
	TypeUnknown    TokenType = iota
	TypeNumber               // 123.45
	TypeString               // "str"
	TypeComment              // #
	TypeColon                // :
	TypeNewline              // \n
	TypeOpenParen            // (
	TypeCloseParen           // )
	TypeOpenBrace            // {
	TypeCloseBrace           // }
	TypeIdent                // names
	TypeEqual                // ==
	TypeNEqual               // !=
	TypeMult                 // *
	TypeMinus                // -
	TypeEOF                  // end-of-file
)

var tokeTypeLabels = map[TokenType]string{
	TypeUnknown:    "Unknown",
	TypeNumber:     "Number",
	TypeString:     "String",
	TypeComment:    "Comment",
	TypeColon:      "Colon",
	TypeNewline:    "Newline",
	TypeOpenParen:  "OpenParen",
	TypeCloseParen: "CloseParen",
	TypeOpenBrace:  "OpenBrace",
	TypeCloseBrace: "CloseBrace",
	TypeIdent:      "Ident",
	TypeEqual:      "Equal",
	TypeNEqual:     "NEqual",
	TypeMult:       "Mult",
	TypeMinus:      "Minus",
	TypeEOF:        "EOF",
}

func (tt TokenType) String() string {
	label, ok := tokeTypeLabels[tt]
	if !ok {
		panic(fmt.Sprintf("unsupported TokenType: %d", tt))
	}
	return label
}

type Token struct {
	Typ TokenType
	Lit string
	Pos int
}

func (t Token) String() string {
	return fmt.Sprintf("%s:%q", t.Typ.String(), t.Lit)
}

func NewToken(tt TokenType, lit string, pos int) Token {
	return Token{
		Typ: tt,
		Lit: lit,
		Pos: pos,
	}
}
