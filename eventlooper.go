package main

import (
	"fmt"
	"os"

	"gitlab.com/siadat/eventlooper/parser"
)

func main() {
	if len(os.Args) < 2 {
		fmt.Printf("usage: eventlooper FILE\n")
		return
	}

	filename := os.Args[1]
	program, err := parser.ParseFile(filename)
	if err != nil {
		fmt.Printf("error: %s\n", err)
		return
	}
	// program.Print()
	scheduler := &Scheduler{}
	err = scheduler.Start(&program, os.Stdout)
	if err != nil {
		fmt.Printf("error: %s\n", err)
		return
	}
}
