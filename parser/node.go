package parser

type Node interface {
	_node()
}

type Statement interface {
	Node
	_statement()
}

type CallStatement struct {
	Name string
	Arg  Expression
}

type GoStatement struct {
	Name string
	Arg  Expression
}

func (CallStatement) _statement() {}
func (GoStatement) _statement()   {}

func (CallStatement) _node() {}
func (GoStatement) _node()   {}
