package parser

import (
	"fmt"
	"io/ioutil"
	"os"
	"strconv"

	"gitlab.com/siadat/eventlooper/scanner"
	"gitlab.com/siadat/eventlooper/tokens"
)

type Parser struct {
	Tokens    []tokens.Token
	FuncDecls []FuncDecl
	Comments  []string

	lastExpect tokens.Token
	cursor     int
	src        string
}

type Program struct {
	FuncDecls []FuncDecl
	Comments  []string
}

// func (p *Program) Print() {
// 	for _, fd := range p.FuncDecls {
// 		fmt.Printf("Function:%s()\n", fd.Name)
// 		for _, stmt := range fd.Statements {
// 			fmt.Printf("  Stmt:%s(%#v)\n", stmt.Name, stmt.Arg)
// 		}
// 	}
// }

func (p *Program) GetFunc(name string) (FuncDecl, error) {
	for _, fd := range p.FuncDecls {
		if name == fd.Name {
			return fd, nil
		}
	}
	return FuncDecl{}, fmt.Errorf("undefined function %q", name)
}

func ParseFile(filename string) (Program, error) {
	f, err := os.Open(filename)
	if err != nil {
		return Program{}, err
	}

	byts, err := ioutil.ReadAll(f)
	if err != nil {
		return Program{}, err
	}

	src := string(byts)
	return ParseSrc(src)
}

func ParseSrc(src string) (Program, error) {
	tokens, err := scanner.ScanSrc(src)
	if err != nil {
		return Program{}, err
	}
	parser := &Parser{
		Tokens: tokens,
		src:    src,
	}
	if err := parser.parseScript(); err != nil {
		return Program{}, err
	}
	return Program{
		FuncDecls: parser.FuncDecls,
		Comments:  parser.Comments,
	}, nil
}

func (p *Parser) parseScript() (err error) {
	defer func() {
		if r := recover(); r != nil {
			if rErr, ok := r.(error); ok {
				err = rErr
			} else {
				err = fmt.Errorf("panic: %s", r)
			}
		}
	}()

	p.FuncDecls = make([]FuncDecl, 0)
	for p.cursor < len(p.Tokens) {
		p.parseFuncDecl()
		if p.next().Typ == tokens.TypeEOF {
			break
		}
	}
	return err
}

func (p *Parser) skip() {
	if p.cursor+1 >= len(p.Tokens) {
		return
	}
	p.cursor++
}

func (p *Parser) current() tokens.Token {
	if p.cursor >= len(p.Tokens) {
		return tokens.Token{}
	}

	return p.Tokens[p.cursor]
}

func (p *Parser) next() tokens.Token {
	var t tokens.Token
	var i int = p.cursor
	for {

		if i >= len(p.Tokens) {
			return tokens.Token{Typ: tokens.TypeEOF}
		}

		t = p.Tokens[i]
		i++

		if t.Typ == tokens.TypeComment {
			p.Comments = append(p.Comments, t.Lit)
		}

		if t.Typ != tokens.TypeNewline && t.Typ != tokens.TypeComment {
			break
		}
	}

	return t
}

func (p *Parser) expect(tt tokens.TokenType) {
	var t tokens.Token

	for {
		if p.cursor >= len(p.Tokens) {
			panic(fmt.Errorf("expected %s; got %s", tt, "end-of-file"))
		}

		t = p.Tokens[p.cursor]
		p.lastExpect = t
		p.cursor++

		if t.Typ == tokens.TypeComment {
			p.Comments = append(p.Comments, t.Lit)
		}

		if t.Typ != tokens.TypeNewline && t.Typ != tokens.TypeComment {
			break
		}
	}

	if t.Typ != tt {
		panic(scanner.AnnotateSrc(p.src, t.Pos, "parse failed", fmt.Sprintf("expected %s; got %s", tt, t)))
	}
}

func (p *Parser) parseStatement() Statement {
	p.expect(tokens.TypeIdent)
	firstToken := p.lastExpect

	if firstToken.Lit == "GO" {
		p.expect(tokens.TypeIdent)
		stmtFuncName := p.lastExpect
		p.expect(tokens.TypeOpenParen)

		arg := p.parseFuncArgValues()
		p.expect(tokens.TypeCloseParen)

		return GoStatement{
			Name: stmtFuncName.Lit,
			Arg:  arg,
		}
	}

	stmtFuncName := p.lastExpect
	p.expect(tokens.TypeOpenParen)

	arg := p.parseFuncArgValues()
	p.expect(tokens.TypeCloseParen)

	return CallStatement{
		Name: stmtFuncName.Lit,
		Arg:  arg,
	}
}

func (p *Parser) parseExpr() Expression {
	switch t := p.current(); t.Typ {
	case tokens.TypeNumber:
		{
			v, err := strconv.Atoi(t.Lit)
			if err != nil {
				panic(scanner.AnnotateSrc(p.src, t.Pos, "parse error", fmt.Sprintf("could not parse int %q: %s", t.Lit, err)))
			}
			p.skip()
			return TypeInt(v)
		}
	case tokens.TypeString:
		{
			v, err := strconv.Unquote(t.Lit)
			if err != nil {
				panic(scanner.AnnotateSrc(p.src, t.Pos, "parse error", fmt.Sprintf("could not parse string %q: %s", t.Lit, err)))
			}
			p.skip()
			return TypeString(v)
		}
	default:
		panic(scanner.AnnotateSrc(p.src, t.Pos, "unsupported token", fmt.Sprintf("expected [%s|%s]; got %s", tokens.TypeNumber, tokens.TypeString, t)))

	}
}

func (p *Parser) parseFuncArgValues() Expression {
	if p.current().Typ == tokens.TypeCloseParen {
		return nil
	}

	return p.parseExpr()
}

func (p *Parser) parseFuncDecl() {
	p.expect(tokens.TypeIdent)
	funcNameToken := p.lastExpect
	p.expect(tokens.TypeOpenParen)
	p.expect(tokens.TypeCloseParen)
	p.expect(tokens.TypeOpenBrace)

	statements := make([]Statement, 0)

	for {
		if p.next().Typ == tokens.TypeCloseBrace {
			break
		}

		stmt := p.parseStatement()
		statements = append(statements, stmt)

		if p.current().Typ == tokens.TypeCloseBrace {
			break
		}
	}

	p.expect(tokens.TypeCloseBrace)

	p.FuncDecls = append(p.FuncDecls, FuncDecl{
		Name:       funcNameToken.Lit,
		Statements: statements,
	})
}
