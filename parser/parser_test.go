package parser

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestParser(t *testing.T) {
	for _, tt := range []struct {
		src  string
		err  error
		want Program
	}{
		{
			src: ``,
			err: nil,
			want: Program{
				FuncDecls: []FuncDecl{},
			},
		},
		{
			src: `func0() {}`,
			err: nil,
			want: Program{
				FuncDecls: []FuncDecl{
					{
						Name:       "func0",
						Statements: []Statement{},
					},
				},
			},
		},
		{
			src: `
			main() { PRINT("123\""); SLEEP(456); func0() }`,
			err: nil,
			want: Program{
				FuncDecls: []FuncDecl{
					{
						Name: "main",
						Statements: []Statement{
							CallStatement{
								Name: "PRINT",
								Arg:  TypeString("123\""),
							},
							CallStatement{
								Name: "SLEEP",
								Arg:  TypeInt(456),
							},
							CallStatement{
								Name: "func0",
								Arg:  nil,
							},
						},
					},
				},
			},
		},
		{
			src: `
			# commend
			func0() {
			  GO ()
			}`,
			err:  fmt.Errorf("panic: parse failed\n\t\t\t  GO ()\n        ^\n        |\n        In line 4, expected Ident; got OpenParen:\"(\"\n"),
			want: Program{},
		},
	} {
		got, err := ParseSrc(tt.src)
		require.Equal(t, tt.err, err)
		require.Equal(t, tt.want, got)
	}
}
