package parser

type FuncDecl struct {
	Name       string
	Statements []Statement
}

func (FuncDecl) _node() {}

// ---

type Expression interface {
	group(TypeIdent, TypeString, TypeInt)
}

type (
	TypeIdent  string
	TypeString string
	TypeInt    int
)

func (TypeString) group(TypeIdent, TypeString, TypeInt) { /* No op */ }
func (TypeInt) group(TypeIdent, TypeString, TypeInt)    { /* No op */ }
func (TypeIdent) group(TypeIdent, TypeString, TypeInt)  { /* No op */ }
